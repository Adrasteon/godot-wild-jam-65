# Overloading
Overloading is a game made for the Godot Wild Jam 65.

## Setup

- Download .NET 8 and add it to `PATH` and `DOTNET_ROOT`
- Add `~/.dotnet/tools` to `PATH`
- Set `DOTNET_CLI_TELEMETRY_OPTOUT` to 1
- Install GodotEnv: `dotnet tool install --global Chickensoft.GodotEnv`
- Run `godotenv godot install 4.2.1`
- Run `godotenv addons install`
- Add a C# script and run the main scene for a first .NET compilation

## Adding an addon
- Add this in addons.jsonc:
  ```
  "imrp": { // name must match the folder name in the repository
        "url": "https://github.com/MakovWait/improved_resource_picker",
        // "source": "remote", // default
        // "checkout": "main", // default
        "subfolder": "addons/imrp"
    }
  ```
- Run `godotenv addons install`

## Setup IDE

### Visual Studio Code
In Editor Settings / Dotnet / Editor:
- Select `Visual Studio Code` in `External Editor`
- Add the path to VS Code in `Custom Exec Path`
- Add `{project} --goto {file}:{line}:{col}` in `Custom Exec Path Args`

In VS Code
- Install the recommended plugins
- Select the `Play` configuration

### Visual Studio
In Editor Settings / Dotnet / Editor:
- Select `Visual Studio` in `External Editor`
- Add the path to Visual Studio in `Custom Exec Path`
- Add `{project} --goto {file}:{line}:{col}` in `Custom Exec Path Args`

In Visual Studio:
- Install this workload: `.NET Desktop Development`
- Select the `Test` configuration

### Rider
In Editor Settings / Dotnet / Editor:
- Select `Jetbrains Rider and Fleet` in `External Editor`
- Add the path to Rider in `Custom Exec Path`
- Add `{project} --goto {file}:{line}:{col}` in `Custom Exec Path Args`

In Rider:
- Install the Godot plugin
- Select the `Player` configuration