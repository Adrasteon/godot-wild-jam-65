#if TOOLS
using System;
using Godot;
using SharpTemplate.addons.custom_nodes.controls;

namespace SharpTemplate.addons.custom_nodes;

[Tool]
public partial class CustomNodesPlugin : EditorPlugin
{
    private const string extendedButtonScriptPath = "res://addons/custom_nodes/controls/ExtendedButton.cs";
    private const string extendedButtonIconPath = "res://addons/custom_nodes/controls/ExtendedButton.svg";
    private const string extendedMarginContainerScriptPath = "res://addons/custom_nodes/controls/ExtendedMarginContainer.cs";
    private const string extendedMarginContainerIconPath = "res://addons/custom_nodes/controls/ExtendedMarginContainer.svg";
    private const string extendedScrollContainerScriptPath = "res://addons/custom_nodes/controls/ExtendedScrollContainer.cs";
    private const string extendedScrollContainerIconPath = "res://addons/custom_nodes/controls/ExtendedScrollContainer.svg";
    private const string extendedRichTextLabelScriptPath = "res://addons/custom_nodes/controls/ExtendedRichTextLabel.cs";
    private const string extendedRichTextLabelIconPath = "res://addons/custom_nodes/controls/ExtendedRichTextLabel.svg";
    private const string extendedHSliderScriptPath = "res://addons/custom_nodes/controls/ExtendedHSlider.cs";
    private const string extendedHSliderIconPath = "res://addons/custom_nodes/controls/ExtendedHSlider.svg";
    
    public override void _EnterTree()
    {
        AddType(extendedButtonScriptPath, extendedButtonIconPath, 
            nameof(ExtendedButton), nameof(Button));
        AddType(extendedMarginContainerScriptPath, extendedMarginContainerIconPath, 
            nameof(ExtendedMarginContainer), nameof(MarginContainer));
        AddType(extendedScrollContainerScriptPath, extendedScrollContainerIconPath, 
            nameof(ExtendedScrollContainer), nameof(ScrollContainer));
        AddType(extendedRichTextLabelScriptPath, extendedRichTextLabelIconPath, 
            nameof(ExtendedRichTextLabel), nameof(RichTextLabel));
        AddType(extendedHSliderScriptPath, extendedHSliderIconPath, 
            nameof(ExtendedHSlider), nameof(HSlider));
    }

    public override void _ExitTree()
    {
        RemoveCustomType(nameof(ExtendedButton));
        RemoveCustomType(nameof(ExtendedMarginContainer));
        RemoveCustomType(nameof(ExtendedScrollContainer));
        RemoveCustomType(nameof(ExtendedRichTextLabel));
        RemoveCustomType(nameof(ExtendedHSlider));
    }

    private void AddType(string scriptPath, string iconPath, string scriptType, string parentType)
    {
        var script = GD.Load<Script>(scriptPath);
        var icon = GD.Load<Texture2D>(iconPath);
        AddCustomType(scriptType, parentType, script, icon);
    }
}
#endif