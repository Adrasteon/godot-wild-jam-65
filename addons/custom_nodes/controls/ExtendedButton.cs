using Godot;

namespace SharpTemplate.addons.custom_nodes.controls;

[Tool]
public partial class ExtendedButton : Button
{
    [Export] private bool removeForWeb = false;
    [Export] private bool initiallyFocused = false;
    private bool hasVisibilityEventHandler = false;
    
    public override void _Ready()
    {
        if (initiallyFocused)
        {
            if(IsVisibleInTree()) this.GrabFocus();
            else
            {
                VisibilityChanged += GrabFocusWhenVisible;
                hasVisibilityEventHandler = true;
            }
        }

        #if GODOT_WEB
        if (removeForWeb)
            QueueFree();
        #endif
    }
    
    private void GrabFocusWhenVisible()
    {
        if (IsVisibleInTree())
        {
            this.GrabFocus();
        }
        else if(hasVisibilityEventHandler)
        {
            VisibilityChanged -= GrabFocusWhenVisible;
            hasVisibilityEventHandler = false;
        }
    }
    
    public override void _ExitTree()
    {
        if (hasVisibilityEventHandler)
        {
            VisibilityChanged -= GrabFocusWhenVisible;
            hasVisibilityEventHandler = false;
        }
    }
}