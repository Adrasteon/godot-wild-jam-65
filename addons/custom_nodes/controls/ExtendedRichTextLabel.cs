using Godot;

namespace SharpTemplate.addons.custom_nodes.controls;

[Tool]
public partial class ExtendedRichTextLabel : RichTextLabel
{
    public override void _EnterTree()
    {
        base._EnterTree();
        MetaClicked += OpenUrl;
    }
    
    public override void _ExitTree()
    {
        base._ExitTree();
        MetaClicked -= OpenUrl;
    }
    
    private void OpenUrl(Variant meta)
    {
        OS.ShellOpen(meta.AsString());
    }
}