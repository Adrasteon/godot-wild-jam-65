using Godot;

namespace SharpTemplate.addons.custom_nodes.controls;

[Tool]
public partial class ExtendedMarginContainer : MarginContainer
{
    [Export] private bool initiallyFocused = false;
    private bool hasVisibilityEventHandler = false;
    
    public override void _Ready()
    {
        if (initiallyFocused)
        {
            if(IsVisibleInTree()) this.GrabFocus();
            else
            {
                VisibilityChanged += GrabFocusWhenVisible;
                hasVisibilityEventHandler = true;
            }
        }
    }
    
    public void SetVisibility(bool visibility)
    {
        Visible = visibility;
    }

    private void GrabFocusWhenVisible()
    {
        if (IsVisibleInTree())
        {
            this.GrabFocus();
        }
        else if(hasVisibilityEventHandler)
        {
            VisibilityChanged -= GrabFocusWhenVisible;
            hasVisibilityEventHandler = false;
        }
    }

    public override void _ExitTree()
    {
        if (hasVisibilityEventHandler)
        {
            VisibilityChanged -= GrabFocusWhenVisible;
            hasVisibilityEventHandler = false;
        }
    }
}