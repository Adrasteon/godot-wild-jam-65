using Godot;

namespace SharpTemplate.addons.custom_nodes.controls;

[Tool]
public partial class ExtendedScrollContainer : ScrollContainer
{
    [Export] private bool withKeyboardXScroll = false;
    [Export] private bool withKeyboardYScroll = false;
    [Export] private int keyboardScrollXSpeed = 100;
    [Export] private int keyboardScrollYSpeed = 100;
    [Export] private bool outlineOnFocus = false;
    private bool hasFocus = false;

    public override void _EnterTree()
    {
        base._EnterTree();

        if (outlineOnFocus)
        {
            Node focusedNode = GetViewport().GuiGetFocusOwner();
            hasFocus = focusedNode == this || focusedNode == GetParent();
            GetViewport().GuiFocusChanged += onFocusChange;
        }
    }
    
    public override void _ExitTree()
    {
        base._ExitTree();
        if (outlineOnFocus)
        {
            GetViewport().GuiFocusChanged -= onFocusChange;
        }
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);
        Node focusedNode = GetViewport().GuiGetFocusOwner();

        if (@event is InputEventKey && @event.IsPressed() &&
            (focusedNode == this || focusedNode == GetParent()))
        {
            InputEventKey keyboardEvent = (InputEventKey)@event;
            
            if (withKeyboardYScroll && keyboardEvent.Keycode == Key.Pagedown)
            {
                ScrollVertical += keyboardScrollYSpeed;
            }
            else if (withKeyboardYScroll && keyboardEvent.Keycode == Key.Pageup)
            {
                ScrollVertical -= keyboardScrollYSpeed;
            }
            else if (withKeyboardXScroll && keyboardEvent.Keycode == Key.Left)
            {
                ScrollHorizontal -= keyboardScrollXSpeed;
            }
            else if (withKeyboardXScroll && keyboardEvent.Keycode == Key.Right)
            {
                ScrollHorizontal += keyboardScrollXSpeed;
            }
        }
    }

    public override void _Draw()
    {
        base._Draw();
        if (hasFocus && outlineOnFocus)
        {
            DrawRect(
                new Rect2(
                    new Vector2(0, 0),
                    this.Size
                ),
                Colors.White,
                false,
                3
            );
        }
    }

    private void onFocusChange(Control control)
    {
        if (outlineOnFocus)
        {
            hasFocus = control == this || control == GetParent();
            QueueRedraw();
        }
    }
}