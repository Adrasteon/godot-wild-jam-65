using Godot;

public class AudioParameters
{
	private const string FileName = "user://prefs.cfg";
	private const string FileSection = "volume";
	private const string MusicVolumeKey = "music";
	private const string SfxVolumeKey = "sfx";
	private const string MusicBus = "Music";
	private const string SfxBus = "SFX";
    
	public static (double, double) GetSavedSoundParams()
	{
		var config = new ConfigFile();
		Error err = config.Load(FileName);
		
		if (err == Error.Ok)
		{
			var currentMusicVolume = (double)config.GetValue(FileSection, MusicVolumeKey, 0.0);
			var currentSfxVolume = (double)config.GetValue(FileSection, SfxVolumeKey, 0.0);

			return (currentMusicVolume, currentSfxVolume);
		}
		else
		{
			return (1, 1);
		}
	}

	public static void SetMusicVolumeToBus(double volume)
	{
		AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex(MusicBus), Mathf.LinearToDb((float)volume));
	}

	public static void SetSfxVolumeToBus(double volume)
	{
		AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex(SfxBus), Mathf.LinearToDb((float)volume));
	}

	public static void SaveSoundParams(double musicVolume, double sfxVolume)
	{
		var config = new ConfigFile();

		config.SetValue(FileSection, MusicVolumeKey, musicVolume);
		config.SetValue(FileSection, SfxVolumeKey, sfxVolume);

		config.Save(FileName);
	}
}
