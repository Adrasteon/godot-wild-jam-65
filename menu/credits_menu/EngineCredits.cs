using System.Collections.Generic;
using System.Text;
using Godot;
using Godot.Collections;

namespace SharpTemplate.menu.credits_menu;

// Inspired by the "About" window of the Godot Game Engine
public partial class EngineCredits : Label
{
	public override void _Ready()
    {
        StringBuilder licenseText = new StringBuilder();

        licenseText.AppendLine(Tr("GODOT_LICENSE"));
        licenseText.AppendLine();
        licenseText.AppendLine(Engine.GetLicenseText());
        
        licenseText.AppendLine("==========================");
        licenseText.AppendLine();
        licenseText.AppendLine(Tr("THIRD_PARTIES"));
        licenseText.AppendLine();
        foreach (Dictionary copyrightInfos in Engine.GetCopyrightInfo())
        {
            licenseText.AppendLine($"- {copyrightInfos["name"]}");

            foreach (Dictionary partInfos in copyrightInfos["parts"].As<Array>())
            {
                foreach (string partCopyright in partInfos["copyright"].As<Array>())
                {
                    licenseText.AppendLine($"    @{partCopyright}");
                }
                licenseText.AppendLine($"    {Tr("LICENSE")}: {partInfos["license"].As<string>()}");
                licenseText.AppendLine();
            }
        }
        
        licenseText.AppendLine("==========================");
        licenseText.AppendLine();
        licenseText.AppendLine(Tr("THIRD_PARTIES_LICENSES"));
        licenseText.AppendLine();
        Dictionary licenses = Engine.GetLicenseInfo();
        foreach (var licenseKey in licenses.Keys)
        {
            licenseText.AppendLine($"- {licenseKey}");
            licenseText.AppendLine();
            licenseText.AppendLine(licenses[licenseKey].As<string>());
        }

        Text = licenseText.ToString();
    }
}