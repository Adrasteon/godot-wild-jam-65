using Godot;

public partial class BackToMenu : Button
{
	public void Back()
	{
		GetTree().ChangeSceneToFile("res://menu/main_menu/Menu.tscn");
	}
}
