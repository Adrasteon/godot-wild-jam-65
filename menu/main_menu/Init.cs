using Godot;

public partial class Init : Node
{
	public override void _Ready()
	{
		var (musicVolume, sfxVolume) = AudioParameters.GetSavedSoundParams();
		AudioParameters.SetMusicVolumeToBus(musicVolume);
		AudioParameters.SetSfxVolumeToBus(sfxVolume);
	}
}
