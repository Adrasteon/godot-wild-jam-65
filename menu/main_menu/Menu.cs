using Godot;

namespace SharpTemplate.menu;

public partial class Menu : Node
{
	public void GoToGame()
	{
		GetTree().ChangeSceneToFile("res://game/GameScene.tscn");
	}
	
	public void GoToCredits()
	{
		GetTree().ChangeSceneToFile("res://menu/credits_menu/Credits.tscn");
	}
	
	public void GoToOptions()
	{
		GetTree().ChangeSceneToFile("res://menu/options_menu/Options.tscn");
	}
	
	public void Quit()
	{
		GetTree().Quit();
	}
}