using Godot;

namespace SharpTemplate.menu.options_menu;

public partial class VolumeSetter : Node
{
	[Export] private HSlider? musicSlider;
	[Export] private HSlider? sfxSlider;
	
	private double currentMusicVolume;
	private double currentSfxVolume;
	
	public override void _Ready()
	{
		base._Ready();
		
		if(musicSlider != null && sfxSlider != null)
		{
			musicSlider.ValueChanged += SetMusicVolume;
			sfxSlider.ValueChanged += SetSfxVolume;
			
			(currentMusicVolume, currentSfxVolume) = AudioParameters.GetSavedSoundParams();
			
			musicSlider.Value = currentMusicVolume;
			sfxSlider.Value = currentSfxVolume;
			
			SetMusicVolume(currentMusicVolume);
			SetSfxVolume(currentSfxVolume);
		}
	}

	private void SetMusicVolume(double volume)
	{
		AudioParameters.SetMusicVolumeToBus(volume);
		currentMusicVolume = volume;
	}

	private void SetSfxVolume(double volume)
	{
		AudioParameters.SetSfxVolumeToBus(volume);
		currentSfxVolume = volume;
	}

	public override void _ExitTree()
	{
		if (musicSlider != null && sfxSlider != null)
		{
			musicSlider.ValueChanged -= SetMusicVolume;
			sfxSlider.ValueChanged -= SetSfxVolume;
		}
		
		AudioParameters.SaveSoundParams(currentMusicVolume, currentSfxVolume);
		
		base._ExitTree();
	}
}